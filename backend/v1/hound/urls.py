from django.conf.urls import include, url
from django.contrib.auth.models import User
from rest_framework import serializers, viewsets, routers
from django.contrib import admin


api_urls = [
    url(r'^lowongan/', include('lowongan.urls', namespace='lowongan')),
    url(r'^profile/', include('user_profile.urls', namespace='profile')),
    url(r'^admin/', include('app_admin.urls', namespace='admin-suling')),
]
# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/', include(api_urls)),
    url(r'^api-auth/', include(
        'rest_framework.urls',
        namespace='rest_framework',
    )),
    url(r'^auth/', include('app_auth.urls', namespace='app-auth')),
    url(r'^register/', include('app_registration.urls', namespace='register')),
]
