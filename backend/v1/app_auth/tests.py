from django.test import TestCase, RequestFactory, Client
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.contrib.sessions.middleware import SessionMiddleware
from django.conf import settings
from django.contrib.auth.models import User
from requests.models import Response

from rest_framework.test import APIRequestFactory, APITestCase

from app_auth.utils import (
    AuthHelper, JWTHelper,
    LinkedinHelper
)
from app_auth.models import UserMahasiswa
from user_profile.models import UserLinkedinProfile, UserCSAuthProfile
from app_auth.views import (
    LinkedinRequestAPIView, requests,
    json, LinkedinCallbackAPIView, GoogleAuthBase,
    GoogleRequestAuthAPIView, GoogleCallbackAuthAPIView,
    LoginMahasiswaAPIView
)
from oauth2client.client import flow_from_clientsecrets
from mock import patch, MagicMock
from datetime import date
from django.contrib.auth.models import User


class AuthHelperTestCase(TestCase):

    def setUp(self):
        self.helper = AuthHelper()
        self.test_username = 'john'
        self.test_password = 'john123'
        self.test_access_token = 'abcdef'
        self.test_user_id = '1231231231'
        self.user = User.objects.create_user(
            "rizal",
            "rizal@gmail.com",
            "rizalkeren"
        )

    def test_get_access_token(self):
        expected = 'username or password is wrong'
        actual = self.helper.get_access_token(
            self.test_username, self.test_password)

        self.assertEqual(actual, expected)

    def test_verify_user(self):
        expected = 'your access_token is invalid'
        actual = self.helper.verify_user(self.test_access_token)
        actual = actual.json()
        for x in actual:
            actual_data = actual[x]

        self.assertEqual(actual_data, expected)

    def test_get_data_user(self):
        expected = 'Authentication credentials were not provided.'
        actual = self.helper.get_user_data(
            self.test_access_token, self.test_user_id)
        for x in actual:
            actual_data = actual[x]

        self.assertEqual(actual_data, expected)

    def test_login_payload_builder(self):
        expected = 'username={}&password={}&grant_type=password'.format(
            self.test_username, self.test_password)

        actual = self.helper.login_payload_builder(
            self.test_username, self.test_password)

        self.assertEqual(actual, expected)

    def test_get_firstname_lastname(self):
        expected = 'Fachrur'
        actual, _ = self.helper.get_firstname_lastname(
            'Fachrur Gaji Magang 10 Juta')

        self.assertEqual(actual, expected)

    def test_get_angkatan_by_npm(self):
        expected = '2015'
        actual = self.helper.get_angkatan_by_npm('1506689143')

        self.assertEqual(actual, expected)

    def test_get_email_by_username(self):
        expected = 'rozijadirektor@ui.ac.id'
        actual = self.helper.get_email_by_username('rozijadirektor')

        self.assertEqual(actual, expected)

    def test_save_user_success(self):
        user_test = self.helper.save_user_profile(
            "me@rizaldntr.com", "rizal", "diantoro")
        user_act = User.objects.get(username="me@rizaldntr.com")
        self.assertEqual(user_act.username, user_test.username)
        self.assertEqual(user_act.first_name, user_test.first_name)
        self.assertEqual(user_act.last_name, user_test.last_name)

    def test_save_user_without_email(self):
        with self.assertRaises(ValueError) as ve:
            self.helper.save_user_profile(None, None, None)

        message = str(ve.exception)
        self.assertEqual(message, "Email tidak boleh kosong!")

    def test_save_user_mahasiswa_success(self):
        user_test = self.helper.save_user_mahasiswa_profile(
            self.user, "1506689055", "Muhammad Rizal Diantoro", "2015")
        user_tmp = User.objects.get(username="rizal")
        user_mahasiswa = UserMahasiswa.objects.get(user=user_tmp)
        self.assertEqual(user_test.npm, user_mahasiswa.npm)
        self.assertEqual(user_test.nama_lengkap, user_mahasiswa.nama_lengkap)
        self.assertEqual(user_test.angkatan, user_mahasiswa.angkatan)
        self.assertEqual(user_test.is_valid, user_mahasiswa.is_valid)

    def test_save_user_mahasiswa_valid(self):
        user_test = self.helper.save_user_mahasiswa_profile(
            self.user, "1506689055", "Muhammad Rizal Diantoro", "2015", True)
        self.assertEqual(user_test.is_valid, True)

    def test_save_user_mahasiswa_failed_user_null(self):
        with self.assertRaises(ValueError) as ve:
            self.helper.save_user_mahasiswa_profile(None, None, "Rizal", None)

        message = str(ve.exception)
        self.assertEqual(message, "Tidak ada user yang sedang aktif!")

    def test_save_user_mahasiswa_failed_fullname_null(self):
        with self.assertRaises(ValueError) as ve:
            self.helper.save_user_mahasiswa_profile(
                self.user, None, None, None)

        message = str(ve.exception)
        self.assertEqual(message, "Nama lengkap tidak boleh kosong!")

    def test_save_user_linkedin_profile_failed(self):
        with self.assertRaises(ValueError) as ve:
            self.helper.save_user_linkedin_profile(
                None, "rizal",
                "diantoro", "gariskepala", "jakarta", "budaklapak",
                1, 1, "hehe summary", "hehe specialties", "url",
                "url2", "rizal@rizal.com")

        message = str(ve.exception)
        self.assertEqual(message, "Tidak ada user yang sedang aktif!")

    def test_save_user_cs_mahasiswa_failed_user_null(self):
        with self.assertRaises(ValueError) as ve:
            self.helper.save_user_csauth_profile(
                None, None, None, None, None, None
            )

        message = str(ve.exception)
        self.assertEqual(message, "Tidak ada user yang sedang aktif!")

    def test_save_user_linkedin_profile(self):
        user_test = self.helper.save_user_linkedin_profile(
            self.user, "rizal", "diantoro", "gariskepala", "jakarta",
            "budaklapak", 1, 1, "hehe summary", "hehe specialties",
            "url", "url2", "rizal@rizal.com")
        user_act = UserLinkedinProfile.objects.get(user=self.user)
        self.assertEqual(user_act.first_name, user_test.first_name)
        self.assertEqual(user_act.last_name, user_test.last_name)
        self.assertEqual(user_act.headline, user_test.headline)
        self.assertEqual(user_act.location, user_test.location)
        self.assertEqual(user_act.industry, user_test.industry)
        self.assertEqual(user_act.current_share, user_test.current_share)
        self.assertEqual(user_act.num_connections, user_test.num_connections)
        self.assertEqual(user_act.summary, user_test.summary)
        self.assertEqual(user_act.specialties, user_test.specialties)
        self.assertEqual(user_act.picture_url, user_test.picture_url)
        self.assertEqual(user_act.public_profile_url,
                         user_test.public_profile_url)
        self.assertEqual(user_act.email_address, user_test.email_address)

    def test_get_prodi_and_program_test_case(self):
        data = {"url": "https://uwantsumtest.cs.ui.ac.id",
                "npm": "1231323",
                "nama": "Lorem Ipsum",
                "alamat_mhs": "Seattle, U.S.A",
                "kd_pos_mhs": "331123",
                "kota_lahir": "Vienna",
                "tgl_lahir": "2017-07-09",
                "program": [
                    {"url": "https://uwantsumtest.cs.ui.ac.id",
                             "periode":
                             {"url": "https://uwantsumtest.cs.ui.ac.id",
                              "term": 1,
                              "tahun": 2017},
                             "kd_org": "01.00.12.01",
                             "nm_org": "Ilmu Komputer",
                             "nm_prg": "S1 Reguler",
                             "angkatan": 2015,
                             "nm_status": "Aktif",
                             "kd_status": "1"},
                    {"url": "https://uwantsumtest.cs.ui.ac.id",
                     "periode":
                     {"url": "https://uwantsumtest.cs.ui.ac.id",
                      "term": 3,
                      "tahun": 2016},
                     "kd_org": "01.00.12.01",
                     "nm_org": "Ilmu Komputer",
                     "nm_prg": "S1 Reguler",
                     "angkatan": 2015,
                     "nm_status": "Kosong",
                     "kd_status": "0"},
                    {"url": "https://uwantsumtest.cs.ui.ac.id",
                     "periode":
                     {"url": "https://uwantsumtest.cs.ui.ac.id",
                      "term": 2,
                      "tahun": 2016},
                     "kd_org": "01.00.12.01",
                     "nm_org": "Ilmu Komputer",
                     "nm_prg": "S1 Reguler",
                     "angkatan": 2015,
                     "nm_status": "Aktif",
                     "kd_status": "1"},
                    {"url": "https://uwantsumtest.cs.ui.ac.id",
                     "periode":
                     {"url": "https://uwantsumtest.cs.ui.ac.id",
                      "term": 1,
                      "tahun": 2016},
                     "kd_org": "01.00.12.01",
                     "nm_org": "Ilmu Komputer",
                     "nm_prg": "S1 Reguler",
                     "angkatan": 2015,
                     "nm_status": "Aktif",
                     "kd_status": "1"},
                    {"url": "https://uwantsumtest.cs.ui.ac.id",
                     "periode":
                     {"url": "https://uwantsumtest.cs.ui.ac.id",
                      "term": 3,
                      "tahun": 2015},
                     "kd_org": "01.00.12.01",
                     "nm_org": "Ilmu Komputer",
                     "nm_prg": "S1 Reguler",
                     "angkatan": 2015,
                     "nm_status": "Kosong",
                     "kd_status": "0"},
                    {"url": "https://uwantsumtest.cs.ui.ac.id",
                     "periode":
                     {"url": "https://uwantsumtest.cs.ui.ac.id",
                      "term": 2,
                      "tahun": 2015},
                     "kd_org": "01.00.12.01",
                     "nm_org": "Ilmu Komputer",
                     "nm_prg": "S1 Reguler",
                     "angkatan": 2015,
                     "nm_status": "Aktif",
                     "kd_status": "1"},
                    {"url": "https://uwantsumtest.cs.ui.ac.id",
                     "periode":
                     {"url": "https://uwantsumtest.cs.ui.ac.id",
                      "term": 1,
                      "tahun": 2015},
                     "kd_org": "01.00.12.01",
                     "nm_org": "Ilmu Komputer",
                     "nm_prg": "S1 Reguler",
                     "angkatan": 2015,
                     "nm_status": "Aktif",
                     "kd_status": "1"}]}
        prodi, program = self.helper.get_prodi_and_program(data)
        self.assertEqual(prodi, "Ilmu Komputer")
        self.assertEqual(program, "S1 Reguler")

    def test_save_user_csauth_profile(self):
        student = UserMahasiswa.objects.create(
            user=self.user,
            nama_lengkap="John Snow",
            npm="1012345678",
            angkatan="2010",
            is_valid=True,
        )

        user_test = self.helper.save_user_csauth_profile(
            student, "Jalan jalan", "Johor Baharu",
            date(2009, 10, 12), "Kedokteran Hewan",
            "S1 Internasional"
        )
        user_act = UserCSAuthProfile.objects.get(user_mahasiswa=student)
        self.assertEqual(user_act.alamat, user_test.alamat)
        self.assertEqual(user_act.kota_lahir, user_test.kota_lahir)
        self.assertEqual(user_act.tanggal_lahir, user_test.tanggal_lahir)
        self.assertEqual(user_act.prodi, user_test.prodi)
        self.assertEqual(user_act.program, user_test.program)


class LinkedinHelperTestCase(TestCase):
    @patch('app_auth.utils.LinkedinHelper.get_random_state')
    def test_get_random_state(self, state):
        state.return_value = 'asdfgh'
        expected = LinkedinHelper().get_random_state()
        actual = 'asdfgh'

        self.assertEqual(expected, actual)


class AuthLinkedinTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.middleware = SessionMiddleware()
        self.request = LinkedinRequestAPIView()
        self.callback = LinkedinCallbackAPIView()

    def test_redirect_to_linkedin(self):
        request = self.factory.get('/auth/linkedin/request')
        self.middleware.process_request(request)
        request.session.save()
        response = self.request.get(request)
        self.assertEqual(response.status_code, 302)

    @patch('app_auth.views.json')
    def test_auth_method_get(self, json_mock):
        request = self.factory.get('/auth/linkedin/callback')
        self.middleware.process_request(request)
        request.session['state'] = ""
        request.session.save()
        json_mock.loads.return_value = (
            {
                'access_token': 'hahaha',
                'expires_in': 1234,
                'emailAddress': 'holaaa',
                'firstName': 'halo',
                'lastName': 'heduue',
                'location': {'name': 'Jakarta'}
            }
        )
        response = self.callback.get(request)
        self.assertEqual(response.status_code, 302)

    @patch('app_auth.views.json')
    def test_auth_rejected(self, json_mock):
        request = self.factory.get(
            '/auth/linkedin/callback?error=user_cancelled' +
            '_authorize&error_description=The+user+cancelled+' +
            'the+authorization&state=987654321')
        self.middleware.process_request(request)
        request.session['state'] = "error found"
        request.session.save()
        json_mock.loads.return_value = (
            {'access_token': 'hahaha', 'expires_in': 1234}
        )
        response = self.callback.get(request)
        self.assertEqual(response.status_code, 401)

    @patch('app_auth.views.json')
    def test_auth_approved(self, json_mock):
        request = self.factory.get(
            '/auth/linkedin/callback?' +
            'code=Akuanaksehat&state=987654321')
        self.middleware.process_request(request)
        request.session['state'] = "987654321"
        request.session.save()
        json_mock.loads.return_value = (
            {
                'access_token': 'hahaha',
                'expires_in': 1234,
                'emailAddress': 'holaaa',
                'firstName': 'halo',
                'lastName': 'heduue',
                'location': {'name': 'Jakarta'},
            }
        )

        response = self.callback.get(request)
        self.assertEqual(response.status_code, 302)

    @patch('app_auth.views.json')
    def test_auth_missmatch_csrf(self, json_mock):
        request = self.factory.get(
            '/auth/linkedin/callback?code' +
            '=Akuanaksehat&state=987654321')
        self.middleware.process_request(request)
        request.session['state'] = "heheheheh"
        request.session.save()
        json_mock.loads.return_value = (
            {'access_token': 'hahaha', 'expires_in': 1234}
        )
        response = self.callback.get(request)
        self.assertEqual(response.status_code, 403)


class GoogleRequestAuthAPIViewTestCase(TestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.google_request = GoogleRequestAuthAPIView()

    def test_get_method(self):
        request = self.factory.get('/auth/google/request')
        response = self.google_request.get(request)
        self.assertEqual(response.status_code, 302)


class GoogleCallbackAuthAPIViewTestCase(TestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.middleware = SessionMiddleware()
        self.google_callback = GoogleCallbackAuthAPIView()
        self.flow = GoogleAuthBase().get_flow()
        self.user = User.objects.create_user(
            "rizal",
            "rizal@gmail.com",
            "rizalkeren"
        )

    @patch('app_auth.views.JWTHelper')
    @patch('app_auth.views.User')
    @patch('app_auth.views.httplib2.Http')
    @patch('app_auth.views.build')
    @patch('oauth2client.client')
    @patch('app_auth.views.flow_from_clientsecrets')
    def test_get_method(
            self, mockflow,
            oauthMock, httpMock,
            buildMock, userMock,
            JWTHelperMock):
        buildMock.return_value("service")
        userMock.objects.get().return_value(self.user)
        oauthMock.authorize.return_value(httpMock)
        response = HttpResponseRedirect('/')
        JWTHelperMock().store_to_cookies(
            response,
            self.user,
            3).return_value(response)
        request = self.factory.get(
            '/auth/google/callback?code=4/P7q7W91a-oMsCeLvIaQm6bTrgtp7'
        )
        self.middleware.process_request(request)
        request.session.save()
        self.google_callback.get(request)
        self.assertEqual(JWTHelperMock.call_count, 2)
        self.assertEqual(response.status_code, 302)

    @patch('app_auth.views.JWTHelper')
    @patch('app_auth.views.httplib2.Http')
    @patch('app_auth.views.build')
    @patch('oauth2client.client')
    @patch('app_auth.views.flow_from_clientsecrets')
    def test_get_method_failed(
            self, mockflow,
            oauthMock, buildMock,
            httpMock, JWTHelperMock):
        people = buildMock.return_value.people.return_value
        people.get().execute.return_value = (
            {
                'emails': [{'value': 'rizaldntr@gmail.com'}],
                'name': {'givenName': '', 'familyName': ''},
                'objectType': 'person',
                'displayName': 'rizallalala'
            }
        )
        oauthMock.authorize.return_value(httpMock)
        response = HttpResponseRedirect('/')
        JWTHelperMock().store_to_cookies(
            response,
            self.user,
            3).return_value(response)
        request = self.factory.get(
            '/auth/google/callback?code=4/P7q7W91a-oMsCeLvIaQm6bTrgtp7'
        )
        self.middleware.process_request(request)
        request.session.save()
        self.google_callback.get(request)
        self.assertEqual(JWTHelperMock.call_count, 2)
        self.assertEqual(response.status_code, 302)

    def test_method_get_with_error(self):
        request = self.factory.get(
            '/auth/google/callback?error=access_denied'
        )
        response = self.google_callback.get(request)
        self.assertEqual(response.status_code, 400)


class LoginMahasiswaAPIViewTestCase(APITestCase):
    url = reverse("app-auth:cs-auth-login")

    def setUp(self):
        self.username = "john"
        self.email = "john@snow.com"
        self.password = "you_know_nothing"
        self.nama_lengkap = "john snow"
        self.user = User.objects.create_user(
            username=self.username, email=self.email, password=self.password)
        self.factory = RequestFactory()

    def test_authentication_without_password(self):
        response = self.client.post(self.url, {"username": "snowman"})
        self.assertEqual(401, response.status_code)

    def test_authentication_with_wrong_password(self):
        response = self.client.post(
            self.url, {"username": self.username, "password": "I_know"})
        self.assertEqual(400, response.status_code)

    def test_authentication_with_valid_data(self):
        response = self.client.post(
            self.url, data={"username": self.username,
                            "password": self.password})
        self.assertEqual(400, response.status_code)

    @patch('app_auth.views.AuthHelper')
    def test_authentication_with_permission_denied(self, AuthHelperMock):
        AuthHelperMock().get_access_token.return_value = "halllosuling"
        AuthHelperMock().verify_user.return_value = {
            'role': 'rizal mau sarjana',
            'identity_number': '1506689055',
            'username': 'muhammad.rizal51'
        }
        requests = self.factory.post('/auth/cs-auth/request')
        requests.data = {"username": self.username, "password": "I_know"}
        response = LoginMahasiswaAPIView().post(requests)
        self.assertEqual(401, response.status_code)

    @patch('app_auth.views.AuthHelper')
    def test_authentication_permission_accepted(self, AuthHelperMock):
        AuthHelperMock().get_access_token.return_value = "halllosuling"

        the_response = Response()
        the_response.code = "success"
        the_response.status_code = 200
        the_response._content = b'{"role": "mahasiswa", \
        "identity_number": "1506689055", \
        "username": "muhammad.rizal51"}'

        AuthHelperMock().verify_user.return_value = the_response
        requests = self.factory.post('/auth/cs-auth/request')
        requests.data = {"username": self.username, "password": "I_know"}
        response = LoginMahasiswaAPIView().post(requests)
        self.assertEqual(401, response.status_code)


class AdminLoginApiViewTestCase(APITestCase):

    def setUp(self):
        self.url = reverse('app-auth:admin-login')
        self.username_admin = 'adminuser'
        self.email_admin = 'admin@cs.ui.ac.id'
        self.password_admin = 'userpassword'
        self.admin = User.objects.create_user(
            username=self.username_admin,
            email=self.email_admin,
            password=self.password_admin,
        )
        self.admin.is_staff = True
        self.admin.save()

        self.username_user = 'adminuser2'
        self.email_user = 'admin2@cs.ui.ac.id'
        self.password_user = 'user2password'

        self.user = User.objects.create_user(
            username=self.username_user,
            email=self.email_user,
            password=self.password_user,
        )
        self.user.is_staff = False
        self.user.save()

    def test_admin_login_accepted(self):
        payload = {
            "username": self.username_admin,
            "email": self.email_admin,
            "password": self.password_admin,
        }
        response = self.client.post(
            self.url, data=payload
        )

        self.assertEquals(200, response.status_code)

    def test_admin_login_forbidden(self):
        payload = {
            "username": self.username_user,
            "email": self.email_user,
            "password": self.password_user,
        }
        response = self.client.post(
            self.url, data=payload
        )

        self.assertEquals(403, response.status_code)

    def test_admin_login_wrong_password(self):
        payload = {
            "username": self.username_user,
            "email": self.email_user,
            "password": 'wrong_password',
        }
        response = self.client.post(
            self.url, data=payload
        )

        self.assertEquals(401, response.status_code)

    def test_admin_login_wrong_user(self):
        payload = {
            "username": "wrong_user",
            "email": self.email_user,
            "password": 'wrong_password',
        }
        response = self.client.post(
            self.url, data=payload
        )

        self.assertEquals(401, response.status_code)


class LogoutMahasiswaAPIViewTestCase(APITestCase):
    url_logout = reverse("app-auth:cs-auth-logout")

    def setUp(self):
        self.username = "john"
        self.email = "john@snow.com"
        self.password = "you_know_nothing"
        self.nama_lengkap = "john snow"
        self.user = User.objects.create_user(
            username=self.username, email=self.email, password=self.password)
        self.factory = RequestFactory()

    def test_logout_without_login(self):
        response = self.client.get(self.url_logout)
        self.assertEqual(401, response.status_code)

    def test_logout_with_login(self):
        self.client.login(username="john", password="you_know_nothing")
        response = self.client.get(self.url_logout)
        self.assertEqual(200, response.status_code)
