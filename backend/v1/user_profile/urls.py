from django.conf.urls import url
from user_profile.views import (
    UserList,
    UserDetail,
    UserDetailEdit,
    UserExperienceEdit,
    UserSkillEdit,
    UserExperienceCreate,
    UserSkillCreate,
    UserLinkedinProfileAPIView,
    UserProfile,
    CSAuthDetails,
)


urlpatterns = [
    url(r'^$', UserList.as_view(), name="list"),
    url(r'^(?P<pk>[0-9]+)/$', UserDetail.as_view(), name="detail"),
    url(r'^cs-profile/(?P<pk>[0-9]+)/$', CSAuthDetails.as_view(), name="cs-detail"),
    url(
        r'^linkedin/(?P<pk>[0-9]+)/$',
        UserLinkedinProfileAPIView.as_view(),
        name="detail-linkedin"
    ),
    url(
        r'^(?P<pk>[0-9]+)/edit/profile/$',
        UserDetailEdit.as_view(),
        name="edit-profile"
    ),
    url(
        r'^(?P<user_pk>[0-9]+)/edit/exp/(?P<pk>[0-9]+)/$',
        UserExperienceEdit.as_view(),
        name="edit-exp"
    ),
    url(
        r'^(?P<user_pk>[0-9]+)/edit/skill/(?P<pk>[0-9]+)/$',
        UserSkillEdit.as_view(),
        name="edit-skill"
    ),
    url(
        r'^(?P<pk>[0-9]+)/add/exp/$',
        UserExperienceCreate.as_view(),
        name="add-exp"
    ),
    url(
        r'^(?P<pk>[0-9]+)/add/skill/$',
        UserSkillCreate.as_view(),
        name="add-skill"
    ),
]
