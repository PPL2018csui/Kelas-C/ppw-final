from rest_framework import serializers
from app_registration.models import UserRegistration


class UserRegistrationSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserRegistration
        fields = (
            'id',
            'username',
            'name',
            'birthday',
            'active_email',
            'angkatan',
            'npm',
            'pa_name',
            'created_at',
        )
