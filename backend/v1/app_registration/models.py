from django.db import models
from django.utils import timezone
import datetime


class UserRegistration(models.Model):

    YEAR_CHOICES = []
    for r in range(1960, (datetime.datetime.now().year+1)):
        YEAR_CHOICES.append((r, r))

    username = models.CharField(max_length=140)
    name = models.CharField(max_length=140)
    birthday = models.DateField()
    active_email = models.EmailField()
    angkatan = models.IntegerField(
        choices=YEAR_CHOICES,
        default=datetime.datetime.now().year
    )
    npm = models.CharField(max_length=10, blank=True, null=True)
    pa_name = models.CharField(max_length=140, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name
