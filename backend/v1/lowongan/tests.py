import json

from django.test import TestCase
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase
from lowongan.models import Job, Perusahaan, JobApplication
from app_auth.models import UserPerusahaan, User, UserMahasiswa
from lowongan.serializers import JobSerializer
from django.contrib.auth.models import User


# Create your tests here.
class LowonganListAPIViewTestCase(APITestCase):
    def setUp(self):
        self.username = "john"
        self.email = "john@snow.com"
        self.password = "you_know_nothing"

        self.company_name = "lorem_ipsum"
        self.company_description = "lorem ipsum dolor sit amet"
        self.company_address = "Jl Jalan"
        self.company_contact = "johny bravo - 08123456789"

        self.job_title = "test job"
        self.job_description = "lorem ipsum dolor sit amet"

        self.user = User.objects.create_user(
            self.username, self.email, self.password
        )

        self.user_perusahaan = UserPerusahaan.objects.create(
            user=self.user,
            nama=self.company_name,
        )

        self.company = Perusahaan()
        self.company.name = self.company_name,
        self.company.description = self.company_description,
        self.company.address = self.company_address,
        self.company.contact_person = self.company_contact,
        self.company.save()

        self.user_perusahaan = self.company

        self.job = Job.objects.create(
            perusahaan=self.company,
            position=self.job_title,
            description=self.job_description,
        )

        self.url = reverse("lowongan:list")
        self.company_url = reverse(
            "lowongan:detail", kwargs={
                "pk": self.company.pk})

    def test_job_object_bundle_authorization(self):
        """
        Test to verify auth in job object bundle
        """
        self.client.login(username="adsasdad", password="aasdadsda")
        response = self.client.get(self.url)
        self.assertEqual(401, response.status_code)

    def test_job_object_bundle(self):
        """
        Test to verify job object bundle
        """
        self.client.login(username=self.username, password=self.password)
        response = self.client.get(self.url)
        self.assertEqual(200, response.status_code)

        job_serializer_data = json.dumps(
            JobSerializer(instance=self.job).data
        )
        job_serializer_data = [json.loads(job_serializer_data)]
        response_data = json.loads(response.content.decode('utf-8'))
        self.assertEqual(job_serializer_data, response_data)


class LowonganDetailsAPIViewTestCase(APITestCase):
    def setUp(self):
        self.username = "john"
        self.email = "john@snow.com"
        self.password = "you_know_nothing"

        self.company_name = "lorem_ipsum"
        self.company_description = "lorem ipsum dolor sit amet"
        self.company_address = "Jl Jalan"
        self.company_contact = "johny bravo - 08123456789"

        self.job_title = "test job"
        self.job_description = "lorem ipsum dolor sit amet"

        self.user = User.objects.create_user(
            self.username, self.email, self.password
        )

        self.user_perusahaan = UserPerusahaan.objects.create(
            user=self.user,
            nama=self.company_name,
        )

        self.company = Perusahaan()
        self.company.name = self.company_name,
        self.company.description = self.company_description,
        self.company.address = self.company_address,
        self.company.contact_person = self.company_contact,
        self.company.save()

        self.user_perusahaan = self.company
        self.user_perusahaan.save()

        self.job = Job.objects.create(
            perusahaan=self.company,
            position=self.job_title,
            description=self.job_description,
        )

        self.url = reverse("lowongan:detail", kwargs={"pk": self.job.pk})

    def test_job_details_authorization(self):
        self.client.login(username="adsad", password="adsda")
        response = self.client.get(self.url)
        self.assertEqual(401, response.status_code)

    def test_job_object_details(self):
        """
        Test to verify job object bundle
        """
        self.client.login(username=self.username, password=self.password)
        response = self.client.get(self.url)
        self.assertEqual(200, response.status_code)

        job_serializer_data = JobSerializer(instance=self.job).data
        response_data = json.loads(response.content.decode('utf-8'))
        self.assertEqual(job_serializer_data, response_data)


class JobApplicationCreateAPIViewTestCase(APITestCase):
    def setUp(self):
        self.username = 'johan'
        self.password = 'asdasdad'
        self.user = User.objects.create_user(
            username=self.username,
            password=self.password,
        )

        self.nama_lengkap = 'Johan Snow'
        self.npm = '1501231234'
        self.angkatan = '2015'
        self.is_valid = True
        self.user_mahasiswa = UserMahasiswa.objects.create(
            user=self.user,
            nama_lengkap=self.nama_lengkap,
            npm=self.npm,
            angkatan=self.angkatan,
            is_valid=self.is_valid
        )

        self.comp_name = 'Traveloke'
        self.comp_desc = 'Jalan-jalan kuy'
        self.comp_address = 'Slipicon Valley'
        self.comp_cp = 'hrd@traveloke.com'
        self.perusahaan = Perusahaan.objects.create(
            name=self.comp_name,
            description=self.comp_desc,
            address=self.comp_address,
            contact_person=self.comp_cp
        )

        self.job_position = 'Data Engineer'
        self.job_location = self.comp_address
        self.job_description = 'Mainan Data'
        self.job = Job.objects.create(
            perusahaan=self.perusahaan,
            position=self.job_position,
            location=self.job_location,
            description=self.job_description,
        )

        self.job_position2 = 'Data Scientist'
        self.job_location2 = self.comp_address
        self.job_description2 = 'Mainan Data'
        self.job2 = Job.objects.create(
            perusahaan=self.perusahaan,
            position=self.job_position2,
            location=self.job_location2,
            description=self.job_description2,
        )

        self.url = reverse("lowongan:apply")
        self.url_lowongan = reverse("lowongan:list")

    def test_apply_without_login(self):
        response = self.client.post(
            self.url, data={"job": self.job.id,
                            "user": self.user_mahasiswa.id,
                            "message": "terima ya hehe",
                            "cv_url": "http://asd.com"})
        self.assertEqual(401, response.status_code)

    def test_apply_with_invalid_job(self):
        self.client.login(username=self.username, password=self.password)
        response = self.client.get(self.url_lowongan)
        self.assertEqual(200, response.status_code)

        response = self.client.post(
            self.url, data={"job": "999",
                            "user": self.user_mahasiswa.id,
                            "message": "terima ya hehe",
                            "cv_url": "http://asd.com"})
        self.assertEqual(400, response.status_code)

    def test_apply_with_invalid_user(self):
        self.client.login(username=self.username, password=self.password)
        response = self.client.get(self.url_lowongan)
        self.assertEqual(200, response.status_code)

        response = self.client.post(
            self.url, data={"job": self.job.id,
                            "user": "999",
                            "message": "terima ya hehe",
                            "cv_url": "http://asd.com"})
        self.assertEqual(400, response.status_code)

    def test_apply_with_valid_data(self):
        self.client.login(username=self.username, password=self.password)
        response = self.client.get(self.url_lowongan)
        self.assertEqual(200, response.status_code)

        response = self.client.post(
            self.url, data={"job": self.job.id,
                            "user": self.user_mahasiswa.id,
                            "message": "terima ya hehe",
                            "cv_url": "http://asd.com"})
        response_data = json.loads(response.content)
        self.assertEqual(201, response.status_code)

    def test_apply_with_same_job_twice(self):
        self.client.login(username=self.username, password=self.password)
        response = self.client.get(self.url_lowongan)
        self.assertEqual(200, response.status_code)

        response = self.client.post(
            self.url, data={"job": self.job.id,
                            "user": self.user_mahasiswa.id,
                            "message": "terima ya hehe",
                            "cv_url": "http://asd.com"})
        response_data = json.loads(response.content)
        self.assertEqual(201, response.status_code)

        response = self.client.post(
            self.url, data={"job": self.job.id,
                            "user": self.user_mahasiswa.id,
                            "message": "terima ya hehe",
                            "cv_url": "http://asd.com"})
        response_data = json.loads(response.content)
        self.assertEqual(400, response.status_code)

    def test_view_applied_job(self):
        self.client.login(username=self.username, password=self.password)
        response = self.client.get(self.url_lowongan)
        self.assertEqual(200, response.status_code)

        response = self.client.post(
            self.url, data={"job": self.job.id,
                            "user": self.user_mahasiswa.id,
                            "message": "terima ya hehe",
                            "cv_url": "http://asd.com"})
        response_data = json.loads(response.content)
        self.assertEqual(201, response.status_code)

        response = self.client.post(
            self.url, data={"job": self.job2.id,
                            "user": self.user_mahasiswa.id,
                            "message": "terima ya hehe",
                            "cv_url": "http://asd.com"})
        response_data = json.loads(response.content)
        self.assertEqual(201, response.status_code)

        response = self.client.get('/api/lowongan/apply/history/')
        response_data = json.loads(response.content)

        self.assertEquals(2, len(response_data))


class LowonganModelsTestCase(TestCase):
    def setUp(self):
        self.username = 'johan'
        self.password = 'asdasdad'
        self.user = User.objects.create_user(
            username=self.username,
            password=self.password,
        )

        self.nama_lengkap = 'Johan Snow'
        self.npm = '1501231234'
        self.angkatan = '2015'
        self.is_valid = True
        self.user_mahasiswa = UserMahasiswa.objects.create(
            user=self.user,
            nama_lengkap=self.nama_lengkap,
            npm=self.npm,
            angkatan=self.angkatan,
            is_valid=self.is_valid
        )

        self.comp_name = 'Traveloke'
        self.comp_desc = 'Jalan-jalan kuy'
        self.comp_address = 'Slipicon Valley'
        self.comp_cp = 'hrd@traveloke.com'
        self.perusahaan = Perusahaan.objects.create(
            name=self.comp_name,
            description=self.comp_desc,
            address=self.comp_address,
            contact_person=self.comp_cp
        )

        self.job_position = 'Data Engineer'
        self.job_location = self.comp_address
        self.job_description = 'Mainan Data'
        self.job = Job.objects.create(
            perusahaan=self.perusahaan,
            position=self.job_position,
            location=self.job_location,
            description=self.job_description,
        )

        self.job_app = JobApplication.objects.create(
            job=self.job,
            user=self.user_mahasiswa,
            message="terima ya hehe",
            cv_url="http://asd.com"
        )

    def test_get_perusahaan_str(self):
        get_str = self.perusahaan.__str__()
        expected = self.perusahaan.name
        self.assertEqual(expected, get_str)

    def test_get_job_str(self):
        get_str = self.job.__str__()
        expected = self.job.position + ' at ' + self.perusahaan.name
        self.assertEqual(expected, get_str)

    def test_get_job_application_str(self):
        get_str = self.job_app.__str__()
        expected = self.job_app.job.position
        self.assertEqual(expected, get_str)
