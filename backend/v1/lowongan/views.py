import json

from django.shortcuts import render
from django.core import serializers
from rest_framework.generics import (
    ListAPIView, RetrieveAPIView, ListCreateAPIView, CreateAPIView
)
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated, IsAdminUser, AllowAny
from rest_framework.response import Response
from lowongan.serializers import (
    JobSerializer,
    JobApplicationSerializer,
)
from lowongan.models import Job, Perusahaan, JobApplication


class LowonganListAPIView(ListAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = JobSerializer
    queryset = Job.objects.all()


class LowonganDetailsAPIView(RetrieveAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = JobSerializer
    queryset = Job.objects.all()


class JobApplicationCreateAPIView(CreateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = JobApplicationSerializer
    queryset = JobApplication.objects.all()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        data = request.data

        return Response(
            data,
            status=201,
            headers=headers
        )


class JobApplicationListAPIView(ListAPIView):
    permission_classes = (IsAdminUser)
    serializer_class = JobApplicationSerializer
    queryset = JobApplication.objects.all()


class AplliedJobListAPIView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        job_apps = JobApplication.objects.filter(user=request.user.id)
        jobs = []

        for app in job_apps:
            serializer = JobSerializer(app.job)
            jobs.append(serializer.data)

        return Response(jobs)
