# from django.test import TestCase, LiveServerTestCase
# from selenium import webdriver
# from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.chrome.options import Options
# from unittest import mock
#
# import os
#
# html_dir = os.path.join(os.getcwd(), "")
#
#
# # Create your tests here.
# class Profil(TestCase):
#
#     def test_homepage_alive(self):
#         response = self.client.get('/mahasiswa/')
#
#         self.assertEqual(response.status_code, 200)
#         self.assertTemplateUsed(response, 'index.html')
#         self.assertContains(response, 'Adam')
#
#
# # Create your tests here.
# class Vacancy(TestCase):
#     # This method will be used by the mock to replace requests.get
#     def mocked_requests_get(*args, **kwargs):
#         class MockResponse:
#             def __init__(self, json_data, status_code):
#                 self.json_data = json_data
#                 self.status_code = status_code
#
#             def json(self):
#                 return self.json_data
#
#         return MockResponse({"key1": "value1"}, 403)
#
#     # This method will be used by the mock to replace requests.get
#     def mocked_requests_backend(*args, **kwargs):
#         class MockResponse:
#             def __init__(self, json_data, status_code):
#                 self.json_data = json_data
#                 self.status_code = status_code
#
#             def json(self):
#                 return self.json_data
#
#         res = [
#           {
#             "id": 27896485,
#             "position": "cillum deserunt nisi Duis ullamco",
#             "description": "eu",
#             "location": "in dolor officia sit",
#             "perusahaan": {
#               "address": "sed",
#               "id": "officia culpa incididunt ",
#               "name": "sed reprehenderit",
#               "company_logo": "sed amet est eu",
#               "contact_person": "Ut pariatur commodo nulla"
#             }
#           },
#           {
#             "id": 31631398,
#             "perusahaan": {
#               "address": "quis adipisicing nulla",
#               "id": "officia cupidat",
#               "name": "proident laboris Ut in",
#               "contact_person": "anim officia commodo mollit",
#               "company_logo": "in"
#             },
#             "location": "consectetur",
#             "description": "nul",
#             "position": "culpa consectetur"
#           }
#         ]
#         return MockResponse(res, 200)
#
#
#
#     @mock.patch('requests.get', side_effect=mocked_requests_get)
#     def test_vacancy_forbidden(self, mock_get):
#         response = self.client.get('/mahasiswa/vacancy/')
#         self.assertContains(response, 'Perboden')
#
#     @mock.patch('requests.get', side_effect=mocked_requests_backend)
#     def test_vacancy_alive(self, mock_get):
#         response = self.client.get('/mahasiswa/vacancy/')
#
#         self.assertEqual(response.status_code, 200)
#         self.assertTemplateUsed(response, 'vacancy.html')
#         self.assertContains(response, 'Adam')
#
#
# # Create your tests here.
# class VacancyDetail(TestCase):
#
#     # This method will be used by the mock to replace requests.get
#     def mocked_requests_backend_details(*args, **kwargs):
#         class MockResponse:
#             def __init__(self, json_data, status_code):
#                 self.json_data = json_data
#                 self.status_code = status_code
#
#             def json(self):
#                 return self.json_data
#
#         res = {
#             "id": 27896485,
#             "position": "cillum deserunt nisi Duis ullamco",
#             "description": "eu",
#             "location": "in dolor officia sit",
#             "perusahaan": {
#               "address": "sed",
#               "id": "officia culpa incididunt ",
#               "name": "sed reprehenderit",
#               "company_logo": "sed amet est eu",
#               "contact_person": "Ut pariatur commodo nulla"
#             }
#           }
#
#         return MockResponse(res, 200)
#
#     @mock.patch('requests.get', side_effect=mocked_requests_backend_details)
#     def test_vacancy_detail_alive(self, mock_get):
#         response = self.client.get('/mahasiswa/vacancy/1')
#
#         self.assertEqual(response.status_code, 200)
#         self.assertTemplateUsed(response, 'vacancy_detail.html')
#         self.assertContains(response, 'Adam')
#
#     # This method will be used by the mock to replace requests.get
#     def mocked_requests_get(*args, **kwargs):
#         class MockResponse:
#             def __init__(self, json_data, status_code):
#                 self.json_data = json_data
#                 self.status_code = status_code
#
#             def json(self):
#                 return self.json_data
#
#         return MockResponse({"key1": "value1"}, 403)
#
#     @mock.patch('requests.get', side_effect=mocked_requests_get)
#     def test_vacancy_detail_forbidden(self, mock_get):
#         response = self.client.get('/mahasiswa/vacancy/1')
#         self.assertContains(response, 'Perboden')
#
#
# class FunctionalProfil(LiveServerTestCase):
#     def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument('--no-sandbox')
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('disable-gpu')
#         self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
#         super(FunctionalProfil, self).setUp()
#
#
#     def mocked_requests_backend(*args, **kwargs):
#         class MockResponse:
#             def __init__(self, json_data, status_code):
#                 self.json_data = json_data
#                 self.status_code = status_code
#
#             def json(self):
#                 return self.json_data
#
#         res = [
#           {
#             "id": 27896485,
#             "position": "cillum deserunt nisi Duis ullamco",
#             "description": "eu",
#             "location": "in dolor officia sit",
#             "perusahaan": {
#               "address": "sed",
#               "id": "officia culpa incididunt ",
#               "name": "sed reprehenderit",
#               "company_logo": "sed amet est eu",
#               "contact_person": "Ut pariatur commodo nulla"
#             }
#           },
#           {
#             "id": 31631398,
#             "perusahaan": {
#               "address": "quis adipisicing nulla",
#               "id": "officia cupidat",
#               "name": "proident laboris Ut in",
#               "contact_person": "anim officia commodo mollit",
#               "company_logo": "in"
#             },
#             "location": "consectetur",
#             "description": "nul",
#             "position": "culpa consectetur"
#           }
#         ]
#         return MockResponse(res, 200)
#
#     def tearDown(self):
#         self.selenium.quit()
#         super(FunctionalProfil, self).tearDown()
#
#     def test_register(self):
#
#         html = "file://" + html_dir+ "app_mahasiswa/profile_mahasiswa.html"
#         selenium = self.selenium
#         # Opening the link we want to test
#         selenium.get(html)
#         # find the form element
#         first_name = selenium.find_element_by_id('searchbar')
#
#         submit = selenium.find_element_by_id('btn-submit')
#
#         # Fill the form with data
#         first_name.send_keys('FrontEnd')
#
#         # submitting the form
#         submit.send_keys(Keys.RETURN)
#
#         first_name = selenium.find_element_by_id('searchbar')
#         # check the returned result
#         self.assertEqual('', first_name.get_attribute('value'))
