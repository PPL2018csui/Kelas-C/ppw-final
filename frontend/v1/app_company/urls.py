from django.conf.urls import url
from .views import *
#url for app
urlpatterns = [
    url(r'^$', show_profile, name='profile'),
    url(r'^id/(?P<key>.*)/$', show_profile_by_id, name='profile-by-id'),
    url(r'^job/add/$', add_job, name='company-add-job'),
    url(r'^job/delete/(?P<id>\d)/$', delete_job, name='company-delete-job'),
]
