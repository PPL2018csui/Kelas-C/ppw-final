#!/bin/sh
python3 manage.py makemigrations
python3 manage.py migrate
/usr/bin/gunicorn ppw_final.wsgi:application -w 2 -b :8000