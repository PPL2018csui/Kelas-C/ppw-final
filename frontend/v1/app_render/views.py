from django.shortcuts import render
from django.contrib import messages
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from .utils import get_data_user

# Create your views here.
response = {}

def login_page(request):
	# print ("#==> masuk index")
	if 'user_login' in request.session:
		username = request.session['user_login']
		response['login'] = True
		return HttpResponseRedirect('/login/' + username)
	else:
		response['author'] = get_data_user(request, 'user_login')
		html = 'login/login_page.html'
		return render(request, html, response)

### SESSION : GET and SET
def get_data_session(request):
	if get_data_user(request, 'user_login'):
		response['author'] = get_data_user(request, 'user_login')

def set_data_for_session(request):
	response['author'] = get_data_user(request, 'user_login')
	response['kode_identitas'] = request.session['kode_identitas']
	response['role'] = request.session['role']

### STATUS
def add_status(request):
	form = Status_Form(request.POST)
	if(request.method == 'POST' and form.is_valid()):
		response['status'] = request.POST['status']
		status = Status(status=response['status'])
		status.save()
		return HttpResponseRedirect('/login_page/dashboard/')
	else:
		return HttpResponseRedirect('/login_page/dashboard/')


def delete_status(request, pk):
	status = Status.objects.filter(pk=pk).first()
	if status != None:
		status.delete()
		pass
	return HttpResponseRedirect('/login_page/dashboard/')

def change_nilai(request):
	if(request.method == 'POST'):
		if(request.POST['show_nilai'] == 'True'):
			response['show_nilai']=True
		elif(request.POST['show_nilai'] == 'False'):
			response['show_nilai']=False
		Show_nilai = show_nilai(reminder=response['show_nilai'])
		Show_nilai.save()
		return HttpResponseRedirect('/login_page/dashboard/')
	else:
		return HttpResponseRedirect('/login_page/dashboard/')
