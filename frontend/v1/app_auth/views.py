from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect

import os

response = {}


def index(request):
    BE_URL = os.getenv('BE_URL', '')
    return render(request, 'login/index.html', {'BE_URL': BE_URL})


def logout(request):
    response = redirect('/auth/')
    response.delete_cookie('jwt_')
    return response


def signup(request):
    return render(request, 'signup/index.html', {})


def verify(request):
    return render(request, 'verification/index.html', {})
