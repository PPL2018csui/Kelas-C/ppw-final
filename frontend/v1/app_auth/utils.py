import os, requests

from django.http import HttpResponseRedirect
from django.urls import reverse

BE_URL = os.getenv('BE_URL', '')


def get_auth(request):
    try:
        jwt_token = request.COOKIES.get("jwt_")
        user_id = request.COOKIES.get("user_id")
    except Exception as e:
        return "", ""
    return jwt_token, user_id


def get_header(f):
    def wrapper(request, *args, **kw):
        jwt_token, _ = get_auth(request)
        header = {'Authorization': 'JWT ' + jwt_token}

        if jwt_token is None:
            return HttpResponseRedirect(reverse('auth:login'))
        else:
            return f(request, header, *args, **kw)  # Call hello

    return wrapper


def fetch(url, header=None):
    endpoint = BE_URL + url
    api_response = requests.get(endpoint, headers=header)
    return api_response

