import React from "react";
import { Router } from "react-router-dom";
import createMemoryHistory from "history/createMemoryHistory";
import { ThemeProvider } from "styled-components";
import { action } from '@storybook/addon-actions';
import theme from "../app/styles/theme.runtime.scss";

const history = createMemoryHistory();

history.push = action("history.push");
history.replace = action("history.replace");
history.go = action("history.go");
history.goBack = action("history.goBack");
history.goForward = action("history.goForward");

export default class Container extends React.Component {
  render() {
    const { story } = this.props;

    return (
      <ThemeProvider theme={theme}>
        <Router history={history}>
          {story()}
        </Router>
      </ThemeProvider>
    );
  }
}
