/*
 * App Actions
 *
 * Actions change things in your application
 * Since this boilerplate uses a uni-directional data flow, specifically redux,
 * we have these actions which are the only way your application interacts with
 * your application state. This guarantees that your state is up to date and nobody
 * messes it up weirdly somewhere.
 *
 * To add a new Action:
 * 1) Import your constant
 * 2) Add a function like this:
 *    export function yourAction(var) {
 *        return { type: YOUR_ACTION_CONSTANT, var: var }
 *    }
 */

import Cookies from "universal-cookie";
import {
  SET_LOADING,
  LOAD_REPOS,
  LOAD_REPOS_SUCCESS,
  LOAD_REPOS_ERROR,
  REQUEST_LOGIN_CS_AUTH,
  REQUEST_LOGIN_CS_AUTH_FAILED,
  REQUEST_LOGIN_CS_AUTH_SUCCESS,
  VERIFY_AUTH_REQUESTED,
  VERIFY_AUTH_FAILED,
  VERIFY_AUTH_SUCCESS,
} from "./constants";

export function setLoading(payload) {
  return {
    type: SET_LOADING,
    payload,
  };
}

/**
 * Load the repositories, this action starts the request saga
 *
 * @return {object} An action object with a type of LOAD_REPOS
 */
export function loadRepos() {
  return {
    type: LOAD_REPOS
  };
}

/**
 * Dispatched when the repositories are loaded by the request saga
 *
 * @param  {array} repos The repository data
 * @param  {string} username The current username
 *
 * @return {object}      An action object with a type of LOAD_REPOS_SUCCESS passing the repos
 */
export function reposLoaded(repos, username) {
  return {
    type: LOAD_REPOS_SUCCESS,
    repos,
    username
  };
}

/**
 * Dispatched when loading the repositories fails
 *
 * @param  {object} error The error
 *
 * @return {object}       An action object with a type of LOAD_REPOS_ERROR passing the error
 */
export function repoLoadingError(error) {
  return {
    type: LOAD_REPOS_ERROR,
    error
  };
}

export function postLoginCSAuth(username, password, requestUrl) {
  return {
    type: REQUEST_LOGIN_CS_AUTH,
    payload: {
      username,
      password,
      requestUrl
    }
  };
}

export function loginCSAuthFailed(payload) {
  return {
    type: REQUEST_LOGIN_CS_AUTH_FAILED,
    payload: {
      payload
    }
  };
}

export function loginCSAuthSuccess(payload) {
  const { jwt, user_id } = payload;
  const cookies = new Cookies();
  cookies.set("jwt_", jwt, { path: "/" });
  cookies.set("user_id", user_id, { path: "/" });
  return {
    type: REQUEST_LOGIN_CS_AUTH_SUCCESS,
    payload: {
      payload
    }
  };
}

export function verifyAuth() {
  return {
    type: VERIFY_AUTH_REQUESTED
  };
}

export function receiveVerifyAuthFailed(payload) {
  return {
    type: VERIFY_AUTH_FAILED,
    payload,
  };
}

export function receiveVerifyAuthSuccess(payload) {
  return {
    type: VERIFY_AUTH_SUCCESS,
    payload,
  };
}
