/*
 * AppConstants
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT';
 */

export const SET_LOADING = 'boilerplate/App/SET_LOADING';
export const LOAD_REPOS = 'boilerplate/App/LOAD_REPOS';
export const LOAD_REPOS_SUCCESS = 'boilerplate/App/LOAD_REPOS_SUCCESS';
export const LOAD_REPOS_ERROR = 'boilerplate/App/LOAD_REPOS_ERROR';
export const VERIFY_AUTH_REQUESTED = 'app/Route/REQUEST_VERIFY_AUTH';
export const VERIFY_AUTH_FAILED = 'app/Route/REQUEST_VERIFY_AUTH_FAILED';
export const VERIFY_AUTH_SUCCESS = 'app/Route/REQUEST_VERIFY_AUTH_SUCCESS';
export const REQUEST_LOGIN_CS_AUTH = 'app/REQUEST_LOGIN_CS_AUTH';
export const REQUEST_LOGIN_CS_AUTH_FAILED = 'app/REQUEST_LOGIN_CS_AUTH_FAILED';
export const REQUEST_LOGIN_CS_AUTH_SUCCESS = 'app/REQUEST_LOGIN_CS_AUTH_SUCCESS';
export const DEFAULT_LOCALE = 'en';
