
import { fromJS } from 'immutable';
import routeReducer from '../reducer';

describe('routeReducer', () => {
  it('returns the initial state', () => {
    expect(routeReducer(undefined, {})).toEqual(fromJS({}));
  });
});
