import React from "react";

import { storiesOf } from "@storybook/react";
import { text, boolean, withKnobs } from "@storybook/addon-knobs";
import { withInfo } from "@storybook/addon-info";
import ProfilePicImage from 'images/user_placeholder@3x.png';

import Image from "./index";

storiesOf("Image", module)
  .addDecorator(withKnobs)
  .add(
    "basic",
    withInfo(``)(() => (
      <Image
        src={ProfilePicImage}
        alt={text("alt text", "example")}
        circled={boolean("circled image", false)}
      />
    ))
  );
