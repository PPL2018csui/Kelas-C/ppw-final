export { default as SlUserFemale } from './user-female';
export { default as SlPeople } from './people';
export { default as SlUserFollow } from './user-follow';
export { default as SlUserFollowing } from './user-following';
export { default as SlUserUnfollow } from './user-unfollow';
export { default as SlUser } from './user';
export { default as SlTrophy } from './trophy';
export { default as SlSpeedometer } from './speedometer';
export { default as SlSocialYoutube } from './social-youtube';
export { default as SlSocialTwitter } from './social-twitter';
export { default as SlSocialTumblr } from './social-tumblr';
export { default as SlSocialFacebook } from './social-facebook';
export { default as SlSocialDropbox } from './social-dropbox';
export { default as SlSocialDribbble } from './social-dribbble';
export { default as SlShield } from './shield';
export { default as SlScreenTablet } from './screen-tablet';
export { default as SlScreenSmartphone } from './screen-smartphone';
export { default as SlScreenDesktop } from './screen-desktop';
export { default as SlPlane } from './plane';
export { default as SlNotebook } from './notebook';
export { default as SlMustache } from './mustache';
export { default as SlMouse } from './mouse';
export { default as SlMagnet } from './magnet';
export { default as SlMagicWand } from './magic-wand';
export { default as SlHourglass } from './hourglass';
export { default as SlGraduation } from './graduation';
export { default as SlGhost } from './ghost';
export { default as SlGameController } from './game-controller';
export { default as SlFire } from './fire';
export { default as SlEyeglass } from './eyeglass';
export { default as SlEnvelopeOpen } from './envelope-open';
export { default as SlEnvolopeLetter } from './envolope-letter';
export { default as SlEnergy } from './energy';
export { default as SlEmotsmile } from './emotsmile';
export { default as SlDisc } from './disc';
export { default as SlCursorMove } from './cursor-move';
export { default as SlCrop } from './crop';
export { default as SlCreditCard } from './credit-card';
export { default as SlChemistry } from './chemistry';
export { default as SlBell } from './bell';
export { default as SlBadge } from './badge';
export { default as SlAnchor } from './anchor';
export { default as SlWallet } from './wallet';
export { default as SlVector } from './vector';
export { default as SlSpeech } from './speech';
export { default as SlPuzzle } from './puzzle';
export { default as SlPrinter } from './printer';
export { default as SlPresent } from './present';
export { default as SlPlaylist } from './playlist';
export { default as SlPin } from './pin';
export { default as SlPicture } from './picture';
export { default as SlMap } from './map';
export { default as SlLayers } from './layers';
export { default as SlHandbag } from './handbag';
export { default as SlGlobeAlt } from './globe-alt';
export { default as SlGlobe } from './globe';
export { default as SlFrame } from './frame';
export { default as SlFolderAlt } from './folder-alt';
export { default as SlFilm } from './film';
export { default as SlFeed } from './feed';
export { default as SlEarphonesAlt } from './earphones-alt';
export { default as SlEarphones } from './earphones';
export { default as SlDrop } from './drop';
export { default as SlDrawar } from './drawar';
export { default as SlDocs } from './docs';
export { default as SlDirections } from './directions';
export { default as SlDirection } from './direction';
export { default as SlDiamond } from './diamond';
export { default as SlCup } from './cup';
export { default as SlCompass } from './compass';
export { default as SlCallOut } from './call-out';
export { default as SlCallIn } from './call-in';
export { default as SlCallEnd } from './call-end';
export { default as SlCalculator } from './calculator';
export { default as SlBubbles } from './bubbles';
export { default as SlBriefcase } from './briefcase';
export { default as SlBookOpen } from './book-open';
export { default as SlBasketLoaded } from './basket-loaded';
export { default as SlBasket } from './basket';
export { default as SlBag } from './bag';
export { default as SlActionUndo } from './action-undo';
export { default as SlActionRedo } from './action-redo';
export { default as SlWrench } from './wrench';
export { default as SlUmbrella } from './umbrella';
export { default as SlTrash } from './trash';
export { default as SlTag } from './tag';
export { default as SlSupport } from './support';
export { default as SlSizeFullscreen } from './size-fullscreen';
export { default as SlSizeActual } from './size-actual';
export { default as SlShuffle } from './shuffle';
export { default as SlShareAlt } from './share-alt';
export { default as SlShare } from './share';
export { default as SlRocket } from './rocket';
export { default as SlQuestion } from './question';
export { default as SlPieChart } from './pie-chart';
export { default as SlPencil } from './pencil';
export { default as SlNote } from './note';
export { default as SlMusicToneAlt } from './music-tone-alt';
export { default as SlMusicTone } from './music-tone';
export { default as SlMicrophone } from './microphone';
export { default as SlLoop } from './loop';
export { default as SlLogout } from './logout';
export { default as SlLogin } from './login';
export { default as SlList } from './list';
export { default as SlLike } from './like';
export { default as SlHome } from './home';
export { default as SlGrid } from './grid';
export { default as SlGraph } from './graph';
export { default as SlEqualizer } from './equalizer';
export { default as SlDislike } from './dislike';
export { default as SlCursor } from './cursor';
export { default as SlControlStart } from './control-start';
export { default as SlControlRewind } from './control-rewind';
export { default as SlControlPlay } from './control-play';
export { default as SlControlPause } from './control-pause';
export { default as SlControlForward } from './control-forward';
export { default as SlControlEnd } from './control-end';
export { default as SlCalender } from './calender';
export { default as SlBulb } from './bulb';
export { default as SlChart } from './chart';
export { default as SlArrowUpCircle } from './arrow-up-circle';
export { default as SlArrowRightCircle } from './arrow-right-circle';
export { default as SlArrowLeftCircle } from './arrow-left-circle';
export { default as SlArrowDownCircle } from './arrow-down-circle';
export { default as SlBan } from './ban';
export { default as SlBubble } from './bubble';
export { default as SlCamrecorder } from './camrecorder';
export { default as SlCamera } from './camera';
export { default as SlCheck } from './check';
export { default as SlClock } from './clock';
export { default as SlClose } from './close';
export { default as SlCloudDownload } from './cloud-download';
export { default as SlCloudUpload } from './cloud-upload';
export { default as SlDoc } from './doc';
export { default as SlEnvolope } from './envolope';
export { default as SlEye } from './eye';
export { default as SlFlag } from './flag';
export { default as SlFolder } from './folder';
export { default as SlHeart } from './heart';
export { default as SlInfo } from './info';
export { default as SlKey } from './key';
export { default as SlLink } from './link';
export { default as SlLock } from './lock';
export { default as SlLockOpen } from './lock-open';
export { default as SlMagnifier } from './magnifier';
export { default as SlMagnifierAdd } from './magnifier-add';
export { default as SlMagnifierRemove } from './magnifier-remove';
export { default as SlPaperClip } from './paper-clip';
export { default as SlPaperPlane } from './paper-plane';
export { default as SlPlus } from './plus';
export { default as SlLocationPin } from './location-pin';
export { default as SlPower } from './power';
export { default as SlRefresh } from './refresh';
export { default as SlReload } from './reload';
export { default as SlSettings } from './settings';
export { default as SlStar } from './star';
export { default as SlSymbleFemale } from './symble-female';
export { default as SlSymbolMale } from './symbol-male';
export { default as SlTarget } from './target';
export { default as SlVolume1 } from './volume-1';
export { default as SlVolume2 } from './volume-2';
export { default as SlVolumeOff } from './volume-off';
export { default as SlPhone } from './phone';
export { default as SlMenu } from './menu';
export { default as SlOptionsVertical } from './options-vertical';
export { default as SlOptions } from './options';
export { default as SlArrowDown } from './arrow-down';
export { default as SlArrowLeft } from './arrow-left';
export { default as SlArrowRight } from './arrow-right';
export { default as SlArrowUp } from './arrow-up';
export { default as SlPaypal } from './paypal';
export { default as SlSocialInstagram } from './social-instagram';
export { default as SlSocialLinkedin } from './social-linkedin';
export { default as SlSocialPintarest } from './social-pintarest';
export { default as SlSocialGithub } from './social-github';
export { default as SlSocialGoogle } from './social-google';
export { default as SlSocialReddit } from './social-reddit';
export { default as SlSocialSkype } from './social-skype';
export { default as SlSocialBehance } from './social-behance';
export { default as SlSocialFoursqare } from './social-foursqare';
export { default as SlSocialSoundcloud } from './social-soundcloud';
export { default as SlSocialSpotify } from './social-spotify';
export { default as SlSocialStumbleupon } from './social-stumbleupon';
export { default as SlMinus } from './minus';
export { default as SlOrganization } from './organization';
export { default as SlExclamation } from './exclamation';
export { default as SlSocialVkontakte } from './social-vkontakte';
export { default as SlEvent } from './event';
export { default as SlSocialSteam } from './social-steam';
