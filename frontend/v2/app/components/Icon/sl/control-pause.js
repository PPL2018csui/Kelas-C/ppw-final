
import React from 'react';
import Icon from 'react-icon-base';

const SlControlPause = props => (
    <Icon viewBox="0 0 1024 1024" {...props}>
        <g><path transform="scale(1, -1) translate(0, -960)" d="M416.272 960h-224.208c-17.6641 0 -32 -14.3203 -32 -32v-960c0 -17.6641 14.3359 -32 32 -32h224.208c17.6797 0 32 14.3359 32 32v960c0 17.6797 -14.3203 32 -32 32zM384.272 0h-160.208v896h160.208v-896zM831.937 960h-223.056c-17.6797 0 -32 -14.3203 -32 -32 v-960c0 -17.6641 14.3203 -32 32 -32h223.056c17.6797 0 32 14.3359 32 32v960c0 17.6797 -14.3037 32 -32 32zM799.937 0h-159.056v896h159.056v-896z"/></g>
    </Icon>
);

export default SlControlPause;
