import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs';
import { withInfo } from "@storybook/addon-info";

import NavbarItem from './index';

storiesOf('NavbarItem', module)
.addDecorator(withKnobs)
.add(
  'basic',
  withInfo(`This is the basic NavbarItem component.`)(() => (
    <NavbarItem>

    </NavbarItem>
  ))
);
