import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs';
import { withInfo } from "@storybook/addon-info";

import CardLowongan from './index';

storiesOf('CardLowongan', module)
.addDecorator(withKnobs)
.add(
  'basic',
  withInfo(`This is the basic CardLowongan component.`)(() => (
    <CardLowongan>

    </CardLowongan>
  ))
);
