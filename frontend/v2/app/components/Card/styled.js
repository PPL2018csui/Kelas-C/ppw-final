import styled from 'styled-components';

export const CardLayout = styled.div`
    background-color: ${(props) => props.background};
    padding: ${(props) => props.padding};
    box-shadow: 0 30px 43px rgba(0, 0, 0, 0.04);
    margin: ${(props) => props.margin};;
`;
