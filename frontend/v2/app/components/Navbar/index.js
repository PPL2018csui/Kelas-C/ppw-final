/**
 *
 * Navbar
 *
 */

import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import autoBind from 'react-autobind';
import Cookies from 'universal-cookie';
import Logo from "images/logo-3-x.png";
import Image from "components/Img";
import profilePlaceholder from "images/user_placeholder.png";

const Nav = styled.nav`
  font-family: "Open Sans", sans-serif;
  border-bottom: 1px solid ${props => props.theme.colorPinkishGrey};
  min-height: ${props => props.theme.navbarHeight};
  z-index: 99;

  .nav__profile-photo {
    width: ${props => props.theme.profilePicSizeSmall};
    height: ${props => props.theme.profilePicSizeSmall};
  }
`;

const cookie = new Cookies();

class Navbar extends React.Component {
  constructor(props) {
    super(props);
    autoBind(this);
    this.state = {
      isExpand: false
    };
  }

  toggleNavbar() {
    this.setState({ isExpand: !this.state.isExpand });
  }

  onLogout(event) {
    event.preventDefault();
    cookie.remove("jwt_", { path: '/'});
    window.location.reload();
  }

  render() {
    const { homeLocation, profileImg, children, user } = this.props;
    const navbarClass = `${
      this.state.isExpand ? "" : "collapse "
    }navbar-collapse`;

    return (
      <Nav className="navbar navbar-expand-lg navbar-light bg-light">
        <a className="navbar-brand ml-4" href={homeLocation}>
          <img height="25" width="auto" src={Logo} alt="logo" />
        </a>
        <button
          className="navbar-toggler"
          type="button"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
          onClick={this.toggleNavbar}
        >
          <span className="navbar-toggler-icon" />
        </button>

        <div className={navbarClass}>
          <ul className="navbar-nav ml-auto">
            {children}
            <li className="nav-item d-flex align-items-center ml-3">
              <a href="/#" onClick={this.onLogout} className="nav-link">
                <div className="row navbar__profile d-flex align-items-center">
                  <div className="mr-4">
                    Hi, <strong>{(user && user.user_mahasiswa && user.user_mahasiswa.nama_lengkap) || 'User'}</strong>
                  </div>
                  <Image
                    circled
                    className="nav__profile-photo"
                    alt="Profile"
                    src={(user && user.profpic_url) ||profileImg}
                  />
                </div>
              </a>
            </li>
          </ul>
        </div>
      </Nav>
    );
  }
}

Navbar.propTypes = {
  homeLocation: PropTypes.string,
  firstname: PropTypes.string,
  children: PropTypes.array,
  profileImg: PropTypes.oneOfType([PropTypes.string, PropTypes.object])
};

Navbar.defaultProps = {
  homeLocation: "#",
  firstname: "User",
  profileImg: profilePlaceholder,
  children: [],
};

export default Navbar;
