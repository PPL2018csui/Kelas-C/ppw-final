/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import React from "react";
import { Helmet } from "react-helmet";
import styled, { ThemeProvider } from "styled-components";
import { Redirect, Switch, Route } from "react-router-dom";

import Navbar from "components/Navbar";
import theme from "styles/theme.runtime.scss";
import { SlHome, SlMagnifier, SlUser } from "components/Icon/sl";
import AppRoute from "components/AppRoute";
import Sidebar from "components/Sidebar";
import Homepage from "app_admin/containers/AdminVerifPage/Loadable";
import LoginPage from "app_admin/containers/AdminLogin";

const Wrapper = styled.div`
  padding: 0;
  #wrapper {
    background-color: ${props => props.theme.colorBluishWhite};
  }

  #main-wrapper {
    padding-left: 0;
    padding-right: 0;
  }

  @media (max-width: 991.99px) {
    #wrapper {
      margin-left: 0;
      margin-right: 0;
    }

    #main-wrapper {
      padding-top: 0 !important;
    }
  }

  @media (min-width: 992px) {
    #wrapper {
      min-height: 100%;
      height: 100%;
      width: 100%;
      position: absolute;
      top: 0;
      left: 0;
      display: block;
      margin: 0;
      padding-top: ${props => props.theme.navbarHeight};
    }

    #main-wrapper {
      height: 100%;
      overflow-y: auto;
    }

    #main {
      position: relative;
      height: 100%;
      overflow-y: auto;
    }

    #sidebar-wrapper {
      height: 100%;
      padding: 0;
      position: fixed;
      border-right: 1px solid ${props => props.theme.colorPinkishGrey};
      background-color: white;
    }

    #sidebar {
      position: relative;
      height: 100%;
      overflow: hidden;
    }
  }
`;

const CompanyLayout = props => (
  <Wrapper className="container-fluid">
    <Navbar />
    <div id="wrapper" className="row">
      <div id="sidebar-wrapper" className="col-lg-3">
        <div id="sidebar" className="col-auto">
          <Sidebar />
        </div>
      </div>
      <div id="main-wrapper" className="col-lg-9 float-right">
        <div id="main">{props.children}</div>
      </div>
    </div>
  </Wrapper>
);

export default function App(props) {
  const { match } = props;
  return (
    <ThemeProvider theme={theme}>
      <div>
        <Helmet titleTemplate="%s - Suling" defaultTitle="Suling App">
          <meta
            name="description"
            content="App for connecting alumni and its former university."
          />
        </Helmet>
        <Switch>
          <AppRoute
            exact
            path={`${match.url}/`}
            component={Homepage}
            layout={CompanyLayout}
          />
          <Route
            path={`${match.url}/login`}
            component={LoginPage}
          />
        </Switch>
      </div>
    </ThemeProvider>
  );
}
