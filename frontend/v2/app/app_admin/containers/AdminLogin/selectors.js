import { createSelector } from 'reselect';

/**
 * Direct selector to the adminLogin state domain
 */
const selectAdminLoginDomain = (state) => state.get('adminLogin');

/**
 * Other specific selectors
 */


/**
 * Default selector used by AdminLogin
 */

const makeSelectAdminLogin = () => createSelector(
  selectAdminLoginDomain,
  (substate) => substate.toJS()
);

export default makeSelectAdminLogin;
export {
  selectAdminLoginDomain,
};
