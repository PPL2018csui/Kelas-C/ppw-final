/*
 *
 * AdminVerifPage actions
 *
 */

import {
  DEFAULT_ACTION,
  GET_REGISTERED_USER_START,
  GET_REGISTERED_USER_SUCCESS,
  GET_REGISTERED_USER_FAIL,
  POST_VERIFY_USER_START,
  POST_VERIFY_USER_SUCCESS,
  POST_VERIFY_USER_FAIL,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function getRegisteredUserStart() {
  return {
    type: GET_REGISTERED_USER_START,
  };
}

export function getRegisteredUserSuccess(payload) {
  return {
    type: GET_REGISTERED_USER_SUCCESS,
    payload,
  };
}

export function getRegisteredUserFail(error) {
  return {
    type: GET_REGISTERED_USER_FAIL,
    error,
  };
}

export function verifyUserStart(payload) {
  return {
    type: POST_VERIFY_USER_START,
    payload,
  };
}

export function verifyUserSuccess(payload) {
  return {
    type: POST_VERIFY_USER_SUCCESS,
    payload,
  };
}

export function verifyUserFail(error) {
  return {
    type: POST_VERIFY_USER_FAIL,
    error,
  };
}
