/**
 *
 * Asynchronously loads the component for AdminVerifPage
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
