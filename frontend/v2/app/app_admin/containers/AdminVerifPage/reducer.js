/*
 *
 * AdminVerifPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  GET_REGISTERED_USER_START,
  GET_REGISTERED_USER_SUCCESS,
  GET_REGISTERED_USER_FAIL,
  POST_VERIFY_USER_START,
  POST_VERIFY_USER_SUCCESS,
  POST_VERIFY_USER_FAIL,
} from './constants';

const initialState = fromJS({
  loading: true,
  payload: null,
});

function adminPageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case GET_REGISTERED_USER_START:
      return state.merge({
        loading: true,
        payload: null,
      });
    case GET_REGISTERED_USER_SUCCESS:
      return state.merge({
        loading: false,
        payload: action.payload,
      });
    case GET_REGISTERED_USER_FAIL:
      return state.merge({
        loading: false,
        error: action.error,
      });
    case POST_VERIFY_USER_START:
      return state.merge({
        loading: true,
        selectedUser: action.payload,
      });
    case POST_VERIFY_USER_SUCCESS:
      return state.merge({
        loading: false,
        selectedUser: action.payload,
      });
    case POST_VERIFY_USER_FAIL:
      return state.merge({
        loading: false,
        error: action.error
      });
    default:
      return state;
  }
}

export default adminPageReducer;
