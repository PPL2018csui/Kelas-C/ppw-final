/**
*
* RegistrantList
*
*/

import React from 'react';
// import styled from 'styled-components';
import PropTypes from 'prop-types';
import RegistrantItem from 'app_admin/components/RegistrantItem/Loadable';
import autobind from 'react-autobind';

class RegistrantList extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    autobind(this);
  }

  handleClickItem(registrant) {
    this.props.handleRegistrant(registrant);
  }

  render() {
    const {
      registrantData,
    } = this.props;

    return (
      <div className="table-responsive">
        <table className="table table-hover">
          <thead>
            <tr>
              <th>Nama</th>
              <th>Npm</th>
              <th>Email</th>
              <th>Created At</th>
            </tr>
          </thead>
          <tbody>
            {registrantData.map((registrant) =>
              <RegistrantItem key={registrant.npm} registrantInfo={registrant} onClickItem={this.handleClickItem} />
            )}
          </tbody>
        </table>
      </div>
    );
  }
}

RegistrantList.propTypes = {
  registrantData: PropTypes.array.isRequired,
  handleRegistrant: PropTypes.func.isRequired,
};

export default RegistrantList;
