import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs';
import { withInfo } from "@storybook/addon-info";

import AdminLoginForm from './index';

storiesOf('AdminLoginForm', module)
.addDecorator(withKnobs)
.add(
  'basic',
  withInfo(`This is the basic AdminLoginForm component.`)(() => (
    <AdminLoginForm
      warnings={{
        username: {
          isWarning: true,
          warningMessage: 'username not found',
        },
        password: {
          isWarning: false,
          warningMessage: '',
        }
      }}
     />
  ))
);
