/**
*
* AdminLoginForm
*
*/

import React from 'react';
import styled from 'styled-components';
import autobind from 'react-autobind';
import PropTypes from 'prop-types';


const Input = styled.input`
`;

const View = styled.div`
  font-family: "Open Sans", sans-serif;
`;

class AdminLoginForm extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
    };

    autobind(this);
  }

  handleChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value,
    });
  }

  handleSubmit(event) {
    const {
      username,
      password,
    } = this.state;

    this.props.submit(username, password);
    event.preventDefault();
  }

  renderWarning(message) {
    return (
      <small id="passwordWarn" className="text-danger">
        {message}
      </small>
    );
  }

  render() {
    const {
      isWarning,
    } = this.props;

    return (
      <View className="center">
        <View className="form-group">
          <Input
            id="inputUsername"
            type="text"
            name="username"
            placeholder="Usename"
            value={this.state.username}
            onChange={this.handleChange}
          /> <br />
        </View>
        <View className="form-group">
          <Input
            id="inputPassword"
            type="password"
            name="password"
            placeholder="Password"
            value={this.state.password}
            onChange={this.handleChange}
          /> <br />
          {!!isWarning && this.renderWarning('Wrong Username or Password')}
        </View>
        <View className="form-group">
          <Input type="submit" value="Login" onClick={this.handleSubmit} />
        </View>
      </View>
    );
  }
}

AdminLoginForm.propTypes = {
  isWarning: PropTypes.bool,
  submit: PropTypes.func,
};

export default AdminLoginForm;
