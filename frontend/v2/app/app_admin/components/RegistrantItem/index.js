/**
*
* RegistrantItem
*
*/

import React from 'react';
// import styled from 'styled-components';
import PropTypes from 'prop-types';
import autobind from 'react-autobind';

class RegistrantItem extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    autobind(this);
  }

  handleClick() {
    this.props.onClickItem(this.props.registrantInfo);
  }

  render() {
    const {
      registrantInfo,
    } = this.props;
    const {
      name,
      npm,
      active_email,
      created_at,
    } = registrantInfo;
    return (
      <tr onClick={this.handleClick}>
        <td>{name}</td>
        <td>{npm}</td>
        <td>{active_email}</td>
        <td>{created_at}</td>
      </tr>
    );
  }
}

RegistrantItem.propTypes = {
  registrantInfo: PropTypes.object.isRequired,
  onClickItem: PropTypes.func.isRequired,
};

export default RegistrantItem;
