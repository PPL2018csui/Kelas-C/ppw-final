import { takeLatest, call, put, select } from 'redux-saga/effects';
import Cookies from 'universal-cookie';
import request from 'utils/request';
import {
  GET_PROFILE_SULING_REQUESTED,
  GET_AUTH_STATE,
  EDIT_PROFILE_SULING_REQUESTED,
} from './constants';
import {
  receiveProfileSulingError,
  receiveProfileSulingSuccess,
  receiveEditProfileSulingError,
  receiveEditProfileSulingSuccess,
  getProfileSuling,
  setAuthState,
} from './actions';

import selectAuthState from 'app_auth/containers/Homepage/selectors';

export function* getProfileSulingSaga(action) {
  const cookies = new Cookies();
  const token = cookies.get('jwt_');
  const header = { Authorization: `JWT ${token}` };
  const requestURL = `${process.env.BE_URL}api/profile/${
    action.payload.userId
  }/`;
  try {
    const response = yield call(request, requestURL, {
      headers: header,
    });
    yield put(
      receiveProfileSulingSuccess({
        user: response,
      })
    );
  } catch (err) {
    yield put(receiveProfileSulingError(err));
  }
}

export function* editProfileSuling(action) {
  const cookies = new Cookies();
  const token = cookies.get('jwt_');
  const header = {
    Authorization: `JWT ${token}`,
    Accept: 'application/json',
    'Content-Type': 'application/json',
  };
  const { id, subtitle, location, phone_no } = action.payload;
  const body = { subtitle, location, phone_no };
  const requestURL = `${process.env.BE_URL}api/profile/${id}/edit/profile/`;
  try {
    const response = yield call(request, requestURL, {
      method: 'PUT',
      headers: header,
      body: JSON.stringify(body),
    });
    yield put(receiveEditProfileSulingSuccess(response));
    yield put(getProfileSuling(id));
  } catch (err) {
    yield put(receiveEditProfileSulingError(err));
  }
}

export function* getAuthState() {
  const authState = yield select(selectAuthState());
  yield put(setAuthState(authState));
}

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield [
    takeLatest(GET_PROFILE_SULING_REQUESTED, getProfileSulingSaga),
    takeLatest(EDIT_PROFILE_SULING_REQUESTED, editProfileSuling),
    takeLatest(GET_AUTH_STATE, getAuthState),
  ];
}
