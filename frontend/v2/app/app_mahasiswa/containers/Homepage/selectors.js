import { createSelector } from 'reselect';

/**
 * Direct selector to the homepage state domain
 */
const selectHomepageDomain = (state) => state.get('app_mahasiswa/homepage');
const selectAppAuthDomain = (state) => state.get('app_auth');

/**
 * Other specific selectors
 */


/**
 * Default selector used by Homepage
 */

const makeSelectHomepage = () => createSelector(
  selectHomepageDomain,
  (substate) => substate.toJS()
);

const makeSelectAppAuth = () => createSelector(
  selectAppAuthDomain,
  (substate) => substate.toJS()
);

export default makeSelectHomepage;
export {
  selectHomepageDomain,
  selectAppAuthDomain,
  makeSelectAppAuth,
  makeSelectHomepage,
};
