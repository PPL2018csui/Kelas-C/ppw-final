/*
 *
 * LowonganPage constants
 *
 */

export const DEFAULT_ACTION = 'app/app_mahasiswa/LowonganPage/DEFAULT_ACTION';
export const FETCH_LOWONGAN_REQUESTED = 'app/app_mahasiswa/LowonganPage/FETCH_LOWONGAN_REQUESTED';
export const FETCH_LOWONGAN_SUCCESS = 'app/app_mahasiswa/LowonganPage/FETCH_LOWONGAN_SUCCESS';
export const FETCH_LOWONGAN_ERROR = 'app/app_mahasiswa/LowonganPage/FETCH_LOWONGAN_ERROR';
export const POST_LAMAR_LOWONGAN_REQUESTED = 'app/app_mahasiswa/LowonganPage/POST_LAMAR_LOWONGAN_REQUESTED';
export const POST_LAMAR_LOWONGAN_SUCCESS = 'app/app_mahasiswa/LowonganPage/POST_LAMAR_LOWONGAN_SUCCESSS';
export const POST_LAMAR_LOWONGAN_ERROR = 'app/app_mahasiswa/LowonganPage/POST_LAMAR_LOWONGAN_ERROR';
export const CLEAR_DATA = 'app/app_mahasiswa/LowonganPage/CLEAR_DATA';
export const FETCH_APPLIED_JOBS_REQUESTED = 'app/app_mahasiswa/LowonganPage/FETCH_APPLIED_JOBS_REQUESTED';
export const FETCH_APPLIED_JOBS_SUCCESS = 'app/app_mahasiswa/LowonganPage/FETCH_APPLIED_JOBS_SUCCESS';
export const FETCH_APPLIED_JOBS_ERROR = 'app/app_mahasiswa/LowonganPage/FETCH_APPLIED_JOBS_ERROR';
