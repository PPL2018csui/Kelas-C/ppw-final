import { call, put, take } from 'redux-saga/effects';
import { takeLatest } from 'redux-saga';
import Cookies from 'universal-cookie';
import request from 'utils/request';
import { FETCH_LOWONGAN_REQUESTED, POST_LAMAR_LOWONGAN_REQUESTED, FETCH_APPLIED_JOBS_REQUESTED } from './constants';
import { receiveLowonganError, receiveLowonganSuccess, receiveLamarLowonganSuccess, receiveLamarLowonganError, receiveAppliedJobsSuccess, receiveAppliedJobsError } from './actions';

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield [
    takeLatest(FETCH_LOWONGAN_REQUESTED, fetchLowonganRequested),
    takeLatest(POST_LAMAR_LOWONGAN_REQUESTED, postLamarLowongan),
    takeLatest(FETCH_APPLIED_JOBS_REQUESTED, fetchAppliedJobs),
  ];
}

export function* fetchLowonganRequested() {
  const cookies = new Cookies();
  const token = cookies.get('jwt_');
  const header = { Authorization: `JWT ${token}` };
  const requestURL = `${process.env.BE_URL}api/lowongan/`;
  try {
    const response = yield call(request, requestURL, {
      headers: header,
    });
    yield put(
      receiveLowonganSuccess(response)
    );
  } catch (err) {
    yield put(receiveLowonganError(err));
  }
}

export function* postLamarLowongan(action) {
  const requestURL = `${process.env.BE_URL}api/lowongan/apply/`;
  const payload = action.payload;
  const cookies = new Cookies();
  const token = cookies.get('jwt_');
  const header = {
    Authorization: `JWT ${token}`,
    Accept: 'application/json',
    'Content-Type': 'application/json',
  };
  try {
    const response = yield call(request, requestURL, {
      method: 'POST',
      headers: header,
      body: JSON.stringify(payload),
    });
    yield put(receiveLamarLowonganSuccess(response));
  } catch (err) {
    yield put(receiveLamarLowonganError(err));
  }
}

export function* fetchAppliedJobs() {
  const cookies = new Cookies();
  const token = cookies.get('jwt_');
  const header = { Authorization: `JWT ${token}` };
  const requestURL = `${process.env.BE_URL}api/lowongan/apply/history/`;
  try {
    const response = yield call(request, requestURL, {
      headers: header,
    });
    yield put(
      receiveAppliedJobsSuccess(response)
    );
  } catch (err) {
    yield put(receiveAppliedJobsError(err));
  }
}

