
import { fromJS } from 'immutable';
import lowonganPageReducer from '../reducer';

describe('lowonganPageReducer', () => {
  it('returns the initial state', () => {
    expect(lowonganPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
