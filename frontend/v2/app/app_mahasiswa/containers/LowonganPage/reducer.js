/*
 *
 * LowonganPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  FETCH_LOWONGAN_SUCCESS,
  POST_LAMAR_LOWONGAN_SUCCESS,
  POST_LAMAR_LOWONGAN_ERROR,
  FETCH_LOWONGAN_ERROR,
  CLEAR_DATA,
  FETCH_APPLIED_JOBS_SUCCESS,
  FETCH_APPLIED_JOBS_ERROR,
} from './constants';

const initialState = fromJS({
  vacancies: [],
  appliedJobs: [],
  success: null,
  error: null,
});

function lowonganPageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case FETCH_LOWONGAN_SUCCESS:
      return state.merge({ vacancies: action.payload });
    case POST_LAMAR_LOWONGAN_SUCCESS:
      return state.merge({ success: action.payload });
    case POST_LAMAR_LOWONGAN_ERROR:
      return state.merge({ error: { lamar: action.err } });
    case FETCH_LOWONGAN_ERROR:
      return state.merge({ error: { lowongan: action.err } });
    case CLEAR_DATA:
      return state.merge({ success: null, error: null });
    case FETCH_APPLIED_JOBS_SUCCESS:
      return state.merge({ appliedJobs: action.payload });
    case FETCH_APPLIED_JOBS_ERROR:
      return state.merge({ error: { applied: action.err } });
    default:
      return state;
  }
}


export default lowonganPageReducer;
