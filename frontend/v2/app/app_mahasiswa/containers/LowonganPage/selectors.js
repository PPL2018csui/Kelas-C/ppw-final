import { createSelector } from 'reselect';

/**
 * Direct selector to the lowonganPage state domain
 */
const selectLowonganPageDomain = (state) => state.get('lowonganPage');
const selectProfileState = (state) => state.get('profilepage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by LowonganPage
 */

const makeSelectLowonganPage = () => createSelector(
  selectLowonganPageDomain,
  (substate) => substate.toJS()
);

const makeProfileState = () => createSelector(
  selectProfileState,
  (substate) => substate.toJS()
);

export default makeSelectLowonganPage;
export {
  selectLowonganPageDomain,
  selectProfileState,
  makeProfileState,
};
