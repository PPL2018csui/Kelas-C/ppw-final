/**
 *
 * LowonganPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import styled from "styled-components";

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectLowonganPage, { makeProfileState } from './selectors';
import reducer from './reducer';
import Cookies from 'universal-cookie';
import profileSaga from 'app_mahasiswa/containers/Homepage/saga';
import profileReducer from 'app_mahasiswa/containers/Homepage/reducer';
import saga from './saga';

import Card from 'components/Card';
import { fetchLowonganRequested, postLamarLowongan, clearDataLamarLowongan, fetchAppliedJobs } from './actions';
import { SlCheck, SlMagnifier, SlClose } from 'components/Icon/sl';
import CardLowongan from 'components/CardLowongan';
import {
  getProfileSuling,
} from 'app_mahasiswa/containers/Homepage/actions';


const MainWrapper = styled.div`
.page__title{
  margin: 1em 0 0 0;
}
.modal{
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: rgba(0,0,0,0.37);
}
.modal_body{
  padding: .5rem;
}
.modal-dialog{
  max-width: 100%;
  flex: .4;
  margin 0;
}
.statusbar__container {
  font-family: 'Open Sans', sans-serif;
  color: #a6a6a6;
  border-radius: .5em; }
.vacancy__container {
  background-color: white;
  border: 1px solid #d1d1d1;
  padding:  0.875em;
  font-family: 'Open Sans', sans-serif;
  position: relative;
  color: #000; }
.vacancy__container:hover {
  cursor: pointer;
  text-decoration: none;
  -webkit-animation: shadow-drop-bottom 0.4s cubic-bezier(0.25, 0.46, 0.45, 0.94) both;
  animation: shadow-drop-bottom 0.4s cubic-bezier(0.25, 0.46, 0.45, 0.94) both; }
.vacancy__container:active {
  -webkit-animation: shadow-inset-center 0.4s cubic-bezier(0.215, 0.61, 0.355, 1) both;
  animation: shadow-inset-center 0.4s cubic-bezier(0.215, 0.61, 0.355, 1) both; }
.vacancy__container .vacancy__company-logo img {
  width: 50px;
  height: 50px;
  border-radius: 50%;
  border: 1px solid #a6a6a6; }
.vacancy__container .vacancy__title {
  font-weight: 800; }
.vacancy__container .vacancy__location {
  margin-top: 0.875em;
  color: #a6a6a6;
  text-align: right;
  font-size: .5em; }
@-webkit-keyframes shadow-drop-bottom {
  0% {
    box-shadow: 0 0 0 0 rgba(0, 0, 0, 0);
    transform: translate(0px, 0px); }
  100% {
    box-shadow: 0 12px 20px -12px rgba(0, 0, 0, 0.35);
    transform: translate(0px, -12px); } }
@keyframes shadow-drop-bottom {
  0% {
    box-shadow: 0 0 0 0 rgba(0, 0, 0, 0);
    transform: translate(0px, 0px); }
  100% {
    box-shadow: 0 12px 20px -12px rgba(0, 0, 0, 0.35);
    transform: translate(0px, -12px); } }

@-webkit-keyframes shadow-inset-center {
  0% {
    box-shadow: inset 0 0 0 0 rgba(0, 0, 0, 0); }
  100% {
    box-shadow: inset 0 0 14px 0px rgba(0, 0, 0, 0.5); } }
@keyframes shadow-inset-center {
  0% {
    box-shadow: inset 0 0 0 0 rgba(0, 0, 0, 0); }
  100% {
    box-shadow: inset 0 0 14px 0px rgba(0, 0, 0, 0.5); } }

`;

export class LowonganPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor() {
    super();
    this.state = { vacancy: null, loading: false };
  }

  componentDidMount() {
    this.props.onGetLowonganSuling();
    this.props.onGetAppliedJobs();
    const cookie = new Cookies();
    const userId = cookie.get('user_id');
    this.props.onGetProfileSuling(userId);
  }

  toggleLamarLowongan(user) {
    const innerToggleLamarLowongan = () => {
      this.props.onPostLamarLowonganSuling(user);
      this.props.onGetAppliedJobs();
    };
    return innerToggleLamarLowongan;
  }

  toggleModal(vacancy) {
    const innerToggleModal = () => {
      this.setState({ vacancy });
    };
    return innerToggleModal;
  }

  closeModal() {
    const innerToggleLamarLowongan = () => {
      this.props.onClearStateLamarLowongan();
      this.setState({ vacancy: null });
    };
    return innerToggleLamarLowongan;
  }

  render() {
    const cookie = new Cookies();
    const userId = cookie.get('user_id');
    return (
      <div>
        <Helmet>
          <title>Lowongan Mahasiswa</title>
          <meta name="description" content="Description of LowonganPage" />
        </Helmet>
        <MainWrapper className="container page__container">
          <div className="row page__header mb-4">
            <div className="col">
              <h1 className="page__title">
                Daftar Lowongan Kerja
            </h1>
            </div>
          </div>
          <div className="row page__body">
            <div className="container">
              <Card className="statusbar__container" backgroundColor="white" padding="1.25em" margin="1em">
                <div className="col">Mendaftar <strong className="text-dark">{this.props.lowonganpage.appliedJobs.length}</strong> Lowongan</div>
              </Card>
              { false && (
                <Card className="filter__container" backgroundColor="#ebf4ff" padding="2rem" margin="1em">
                  <form className="row align-items-stretch mb-3">
                    <div className="col">
                      <div className="input-group input h-100">
                        <div className="input-group-prepend input__prepend-icon">
                          <span className="input-group-text"><SlMagnifier /> </span>
                        </div>
                        <input id="searchbar" title="keyword" type="text" className="form-control" />
                        <div className="dropdown input-group-append">
                          <button className="btn btn-default dropdown-toggle" id="dm-1" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Position
                        </button>
                          <div className="dropdown-menu" aria-labelledby="dm-1">
                            <a className="dropdown-item" href="#!">Position</a>
                            <a className="dropdown-item" href="#!">Company</a>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-auto">
                      <button type="submit" id="btn-submit" className="btn btn-primary btn-flat">Search</button>
                    </div>
                  </form>
                </Card>
              )}
              
              <Card className="row vacancies__container" backgroundColor="white" padding="1rem 1.5rem" margin="1em">
                <div className="container">
                  {this.props.lowonganpage.vacancies.map((vacancy) => (
                    <a key={vacancy.position + vacancy.perusahaan.name} onClick={this.toggleModal(vacancy)} href="#!">
                      <CardLowongan vacancy={vacancy} />
                    </a>
                    )
                  )}

                </div>
              </Card>
            </div>
          </div>
          {this.state.vacancy && (
            <div className="modal" id="exampleModalVertical" tabIndex="-1">
              <div className="modal-dialog" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title" id="exampleModalVerticalLabel"><strong>{this.props.lowonganpage.success ? 'Berhasil Melamar' : this.props.lowonganpage.error ? 'Error' : 'Detail Lowongan'}</strong></h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.props.lowonganpage.success ? null : this.closeModal()}>
                      {this.props.lowonganpage.success || this.props.lowonganpage.error ? (<div></div>) : (
                        <span aria-hidden="true">&times;</span>
                      )}
                    </button>
                  </div>
                  <div className="modal-body">
                    <div className="container">
                      {this.props.lowonganpage.success ? (
                        <div>
                          <div className="row modal_body">
                            <div className="col-md-12 text-center"><SlCheck size={100} color="Green" /></div>
                            <div className="col-md-12 text-center">Kamu berhasil melamar lowongan</div>
                          </div>
                          <div className="row modal_body">
                            <div className="col-md-12 col-12">
                              <CardLowongan vacancy={this.state.vacancy} />
                            </div>
                          </div>
                        </div>
                      ) : this.props.lowonganpage.error ? (
                        <div>
                          <div className="row modal_body">
                            <div className="col-md-12 text-center"><SlClose size={100} color="Red" /></div>
                              <div className="col-md-12 text-center">Gagal Melamar Lowongan</div>
                          </div>
                          <div className="row modal_body">
                            <div className="col-md-12 col-12">
                              
                            </div>
                          </div>
                        </div>
                      ) : (
                        <div>
                          <div className="row modal_body">
                            <div className="col-md-4 col-12">Posisi</div>
                            <div className="col">{this.state.vacancy.position}</div>
                          </div>
                          <div className="row modal_body">
                            <div className="col-md-4 col-12">Deskripsi</div>
                            <div className="col">
                              <textarea className="form-control" readOnly defaultValue={this.state.vacancy.description} />
                            </div>
                          </div>
                        </div>
                      )}
                    </div>
                  </div>
                  {this.props.lowonganpage.error ? (
                    <div className="modal-footer">
                      <button type="button" className="btn btn-primary" onClick={this.closeModal()}>Tutup</button>
                    </div>
                  ) : (
                    <div className="modal-footer">
                      <button type="button" className="btn btn-primary" onClick={this.props.lowonganpage.success ? this.closeModal() : this.toggleLamarLowongan({ user: parseInt(userId, 10), job: this.state.vacancy.id })}>{this.props.lowonganpage.success ? 'Selesai' : 'Lamar Lowongan'}</button>
                    </div>
                  )}

                </div>
              </div>
            </div>
          )}

        </MainWrapper>
      </div>
    );
  }
}

LowonganPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
  lowonganpage: PropTypes.shape({
    vacancies: PropTypes.array,
    success: PropTypes.object,
    error: PropTypes.object,
    appliedJobs: PropTypes.array,
  }),
  profilepage: PropTypes.shape({
    payload: PropTypes.shape({
      user: PropTypes.shape({
        user_mahasiswa: PropTypes.object,
      }),
    }),
  }),

};

const mapStateToProps = createStructuredSelector({
  lowonganpage: makeSelectLowonganPage(),
  profilepage: makeProfileState(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    onGetLowonganSuling: () => dispatch(fetchLowonganRequested()),
    onGetAppliedJobs: () => dispatch(fetchAppliedJobs()),
    onPostLamarLowonganSuling: (user) => dispatch(postLamarLowongan(user)),
    onClearStateLamarLowongan: () => dispatch(clearDataLamarLowongan()),
    onGetProfileSuling: (userId) => dispatch(getProfileSuling(userId)),
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'lowonganPage', reducer });
const withSaga = injectSaga({ key: 'lowonganPage', saga });

const withProfileReducer = injectReducer({
  key: 'profilepage',
  reducer: profileReducer,
});

const withAuthProfile = injectSaga({ key: 'profilepage', saga: profileSaga });

export default compose(
  withReducer,
  withSaga,
  withProfileReducer,
  withAuthProfile,
  withConnect,
)(LowonganPage);
