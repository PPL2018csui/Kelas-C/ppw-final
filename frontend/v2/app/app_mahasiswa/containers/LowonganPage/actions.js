/*
 *
 * LowonganPage actions
 *
 */

import {
  DEFAULT_ACTION,
  FETCH_LOWONGAN_SUCCESS,
  FETCH_LOWONGAN_REQUESTED,
  FETCH_LOWONGAN_ERROR,
  POST_LAMAR_LOWONGAN_SUCCESS,
  POST_LAMAR_LOWONGAN_ERROR,
  POST_LAMAR_LOWONGAN_REQUESTED,
  CLEAR_DATA,
  FETCH_APPLIED_JOBS_REQUESTED,
  FETCH_APPLIED_JOBS_SUCCESS,
  FETCH_APPLIED_JOBS_ERROR,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function receiveLowonganSuccess(payload) {
  return {
    type: FETCH_LOWONGAN_SUCCESS,
    payload,
  };
}

export function receiveLowonganError(err) {
  return {
    type: FETCH_LOWONGAN_ERROR,
    err,
  };
}

export function fetchLowonganRequested() {
  return {
    type: FETCH_LOWONGAN_REQUESTED,
  };
}

export function receiveLamarLowonganSuccess(payload) {
  return {
    type: POST_LAMAR_LOWONGAN_SUCCESS,
    payload,
  };
}

export function receiveLamarLowonganError(err) {
  return {
    type: POST_LAMAR_LOWONGAN_ERROR,
    err,
  };
}

export function postLamarLowongan(payload) {
  return {
    type: POST_LAMAR_LOWONGAN_REQUESTED,
    payload,
  };
}

export function clearDataLamarLowongan() {
  return {
    type: CLEAR_DATA,
  };
}

export function fetchAppliedJobs() {
  return {
    type: FETCH_APPLIED_JOBS_REQUESTED,
  };
}


export function receiveAppliedJobsSuccess(payload) {
  return {
    type: FETCH_APPLIED_JOBS_SUCCESS,
    payload,
  };
}

export function receiveAppliedJobsError(err) {
  return {
    type: FETCH_APPLIED_JOBS_ERROR,
    err,
  };
}

